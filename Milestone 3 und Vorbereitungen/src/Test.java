import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<String>();

		String word = "Alec";
		
		list.add("Adolf");
		list.add("Bdolf");
		list.add("Cdolf");
		list.add("Rudolf");
		list.add("Lilano");
		list.add("Alec");
		list.add("Valestra");
		
		System.out.println(Test.suche(list,word));
		Test.bubblesort(list);
		
		for(int i = 0; i<list.size();i++) {
			System.out.println(list.get(i));
		}
		
	}

	
	public static int suche(ArrayList<String> list, String word) {
			for(int i = 0; i < list.size();i++) {
				if (list.get(i).equals(word)) 
					return i;
			}
			return -1;
		}
	
	
	public static void bubblesort(ArrayList<String> list) {
		
		
		String temp = null;
		for(int i = 1; i < list.size();i++){
			for(int j = 0; j < list.size()-1;j++) {
				if(list.get(j).compareTo(list.get(j + 1)) > 0) {
					temp = list.get(j);
					list.set(j, list.get(j+1));
					list.set(j+1, temp);
				}
				
			}
		}
	}

}
