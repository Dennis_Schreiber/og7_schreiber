

public class KeyStore01 extends java.lang.Object{

	private final int LENGTH = 100;
	private String[] keys = new String[LENGTH];
	
	public KeyStore01(){
	}
	
	public KeyStore01(int length) {
		this.keys = new String[length];
		
	}
	
	public boolean add(java.lang.String e) {
		for(int i = 0; i<this.keys.length;i++) {
			if(this.keys[i] == null) {
				this.keys[i] = e;
				return true;
			}
		}
		return false;
	}
	
	public void clear() {
		for(int i = 0;i<this.keys.length;i++) {
			this.keys[i] = null;
		}
	}
	
	public java.lang.String get(int index){
		String non ="";
		for(int i = 0;i<this.keys.length;i++) {
			non = this.keys[index];
		}
		return non;
	}
	
	public int indexOf(String eintrag) {
		for(int i = 0; i < this.keys.length; i++) {
			if(eintrag.equals(keys[i])) {
			//if(this.keys[i].equals(eintrag)) {
				return i;
			}
		}
		return -1;
	}  
	
	public boolean remove(int index) {
		for(int i= 0;i<this.keys.length;i++) {
			if(i == index) {
				this.keys[i] = null;
				return true;
			}
		}
		return false;
	}

	public boolean remove(String eintrag) {
		for(int i= 0;i<this.keys.length;i++) {
			if(this.keys[i].equals(eintrag)) {
				this.keys[i]= null;
				return true;
			}
		}
		return false;
	}
	
	public int size() {
		int anzahl = 0;
		for(int i = 0;i<this.keys.length;i++) {
			if(this.keys[i] != null) {
			anzahl = anzahl + 1;	
			}
		}
		return anzahl;
	}
	
	public java.lang.String toString(){
		String endstr = "";
		  for(int i = 0;i<this.keys.length;i++) {
			if(this.keys[i] != null) {
			endstr = endstr + this.keys[i] + " | " ;
		     }
		  }	
		return endstr;
	}
}
