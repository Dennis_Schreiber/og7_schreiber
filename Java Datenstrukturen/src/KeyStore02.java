import java.util.ArrayList;

public class KeyStore02 {

	ArrayList<String> keys = new ArrayList<String>();
	
	 public KeyStore02() {
	 }
	
	 public boolean add(String eintrag) {
		keys.add(eintrag);
		 return true;
	 }
	 
	 public int indexOf(String eintrag) {
		 return keys.indexOf(eintrag);
	 }
	 
	 public void remove(int index) {
		 keys.remove(index);
	 }
	 
	 public void remove(String eintrag) {
		 keys.remove(eintrag);
	 }
	 
	 public void clear() {
		 keys.clear();
	 }
	 
	 public String get(int index) {
		 return keys.get(index);
	 }
	 
	 public int size() {
		return keys.size();
	 }
	 
	 public String toString() {
		 return keys.toString();
	 }
}
