import java.util.Arrays;

/**
  *
  * Übungsklasse zu Feldern
  *
  * @version 1.0 vom 05.05.2011
  * @author Tenbusch
  */

public class Felder {

  //unsere Zahlenliste zum Ausprobieren
  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
  
  //Konstruktor
  public Felder(){}

  //Methode die Sie implementieren sollen
  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
  
  //die Methode soll die größte Zahl der Liste zurückgeben
  public int maxElement(){
	 Arrays.sort(zahlenliste);
	 int maximum = zahlenliste.length - 1;	  
	  return maximum;
  }

  //die Methode soll die kleinste Zahl der Liste zurückgeben
  public int minElement(){
	  Arrays.sort(zahlenliste);
		 int minimum = zahlenliste[0];	  
		  return minimum;
  }
  
  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zurückgeben
  public double durchschnitt(){
   double summe = 0;
	  for(int i = 0; i<zahlenliste.length;i++) {
		 	
	  summe = summe + zahlenliste[i];
	   
	  }
    
	  summe = summe / zahlenliste.length;
	  return summe;
  }

  //die Methode soll die Anzahl der Elemente zurückgeben
  //der Befehl zahlenliste.length; könnte hierbei hilfreich sein
  public int anzahlElemente(){
    int anzahl = zahlenliste.length;
	  return anzahl;
  }

  //die Methode soll die Liste ausgeben
  public String toString(){
	  String zahlen = "";
    for(int i = 0;i<zahlenliste.length;i++)
	  zahlen = zahlen +  zahlenliste[i] + ", ";
	  return zahlen;
  }

  //die Methode soll einen booleschen Wert zurückgeben, ob der Parameter in
  //dem Feld vorhanden ist
  public boolean istElement(int zahl){
	  boolean wert = true;
    for(int i = 0; i<zahlenliste.length;i++)
    	if (zahlenliste[i] == zahl) {
    		wert = true;
    	}
    	else {
    		wert = false;
    	}
	  return wert;
  }
  
  //die Methode soll das erste Vorkommen der
  //als Parameter übergebenen  Zahl liefern oder -1 bei nicht vorhanden
  public int getErstePosition(int zahl){
    return 0;
  }
  
  //die Methode soll die Liste aufsteigend sortieren
  //googlen sie mal nach Array.sort() ;)
  public void sortiere(){
	  Arrays.sort(zahlenliste);
  }

  public static void main(String[] args) {
    Felder testenMeinerLösung = new Felder();
    System.out.println(testenMeinerLösung.maxElement());
    System.out.println(testenMeinerLösung.minElement());
    System.out.println(testenMeinerLösung.durchschnitt());
    System.out.println(testenMeinerLösung.anzahlElemente());
    System.out.println(testenMeinerLösung.toString());
    System.out.println(testenMeinerLösung.istElement(0));
    System.out.println(testenMeinerLösung.getErstePosition(5));
    testenMeinerLösung.sortiere();
    System.out.println(testenMeinerLösung.toString());
  }
}
