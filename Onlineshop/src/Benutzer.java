
public abstract class Benutzer {

	private long benutzerID;
	private String passwort;
	private  String name;
	private String email;
	
	
	public Benutzer(long benutzerID, String passwort, String name, String email) {
		super();
		this.benutzerID = benutzerID;
		this.passwort = passwort;
		this.name = name;
		this.email = email;
	}
	public long getBenutzerID() {
		return benutzerID;
	}
	public void setBenutzerID(long benutzerID) {
		this.benutzerID = benutzerID;
	}
	public String getPasswort() {
		return passwort;
	}
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean pr�feID(long benutzerID) {
		boolean gg = false;
		if(benutzerID > 100) {
			gg = true;
		}
		 
		return gg;
	}
}
