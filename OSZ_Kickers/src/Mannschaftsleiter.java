
public class Mannschaftsleiter extends Spieler{

	//Atribute
	private String mannschaftsname;
	private int rabbat;
	
	
	//Methoden
	public Mannschaftsleiter(String vorname, String name, String telefonnummer, boolean istBeitrag, int trikotnummer,
			String spielerposition, String mannschaftsname, int rabbat) {
		super(vorname, name, telefonnummer, istBeitrag, trikotnummer, spielerposition);
		this.mannschaftsname = mannschaftsname;
		this.rabbat = rabbat;
	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public int getRabbat() {
		return rabbat;
	}

	public void setRabbat(int rabbat) {
		this.rabbat = rabbat;
	}
	
	public String liefereDaten() {
		String data ="";
		data = "Name: " + getVorname() + " " + getName() + "|" + "Telefonnummer: " + getTelefonnummer() + "|" + "Beitrag bezahlt? : " + isIstBeitrag()
		 + "|" +"Trikotnummer: "+ getTrikotnummer()  + "|" +"Spielerposition: " + getSpielerposition()  + "|" + "Mannschaftsname: " + mannschaftsname  + "|" + "Rabbat: " + rabbat + "%";
		return data;
	}
	
}
