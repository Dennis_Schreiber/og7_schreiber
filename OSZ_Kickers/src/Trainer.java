
public class Trainer extends Person {

	//Attribute
	private char lizenzklasse;
	private int entschaedigung;
	private char spielklasse;
	
	//Methoden
	public Trainer(String vorname, String name, String telefonnummer, boolean istBeitrag, char lizenzklasse,
			int entschaedigung, char spielklasse) {
		super(vorname, name, telefonnummer, istBeitrag);
		this.lizenzklasse = lizenzklasse;
		this.entschaedigung = entschaedigung;
		this.spielklasse = spielklasse;
	}
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getEntschaedigung() {
		return entschaedigung;
	}

	public void setEntschaedigung(int entschaedigung) {
		this.entschaedigung = entschaedigung;
	}

	public char getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklassse(char spielklasse) {
		this.spielklasse = spielklasse;
	}
	
	public String liefereDaten() {
		String data ="";
		data = "Name: " + getVorname() + " " + getName() + "|" + "Telefonnummer: " + getTelefonnummer() + "|" + "Beitrag bezahlt? : " + isIstBeitrag()
		+ "|" + "lizenzklasse: " + lizenzklasse + "|" +  "Entschaedigung: " + entschaedigung + "|" + "Spielklasse: " + spielklasse;
		return data;
	}

}
