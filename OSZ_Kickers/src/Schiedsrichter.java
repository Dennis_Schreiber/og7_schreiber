
public class Schiedsrichter extends Person {


	//Attribute
	private int gepfiffeneSpiele;

	//Methoden
	
	public Schiedsrichter(String vorname, String name, String telefonnummer, boolean istBeitrag, int gepfiffeneSpiele) {
		super(vorname, name, telefonnummer, istBeitrag);
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public String liefereDaten() {
		String data ="";
		data = "Name: " + getVorname() + " " + getName() + "|" + "Telefonnummer: " + getTelefonnummer() + "|" + "Beitrag bezahlt? : " + isIstBeitrag()
		 + "|" + "Anzahl der gepfiffenen Spiele: " + gepfiffeneSpiele;
		return data;
	}
	
	
}
