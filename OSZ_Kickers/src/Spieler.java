
public class Spieler extends Person{

	//Attribute
	private int trikotnummer;
	private String spielerposition;
	
	//Methoden
	
	public Spieler(String vorname, String name, String telefonnummer, boolean istBeitrag, int trikotnummer, String spielerposition) {
		super(vorname, name, telefonnummer, istBeitrag);
		this.trikotnummer = trikotnummer;
		this.spielerposition = spielerposition;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielerposition() {
		return spielerposition;
	}

	public void setSpielerposition(String spielerposition) {
		this.spielerposition = spielerposition;
	}

	public String liefereDaten() {
		String data ="";
		data = "Name: " + getVorname() + " " + getName() + "|" + "Telefonnummer: " + getTelefonnummer() + "|" + "Beitrag bezahlt? : " + isIstBeitrag()
		 + "|" + "Trikotnummer: "+ trikotnummer  + "|" + "Spielerposition: " + spielerposition;
		return data;
	}
}
