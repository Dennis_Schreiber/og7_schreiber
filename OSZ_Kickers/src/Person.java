
public abstract class Person {

	//Attribute
	private String vorname;
	private String name;
	private String telefonnummer;
	private boolean istBeitrag;
	
	
	//Methoden
	
	public Person(String vorname, String name, String telefonnummer, boolean istBeitrag) {
		this.vorname = vorname;
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.istBeitrag = istBeitrag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isIstBeitrag() {
		return istBeitrag;
	}
	public void setIstBeitrag(boolean istBeitrag) {
		this.istBeitrag = istBeitrag;
	}
	
	public abstract String liefereDaten();
	
	
	
}
