import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DateiEinlesen {

	
	
	
	public static Lerneintrag ladeDatei(String datName) throws ParseException {

		 File file = new File(datName);
		 //FileReader F1 = new FileReader("C:\\Users\\user\\git\\og7_schreiber\\Lerntagebuch\\src\\miriam.dat");

	        if (!file.canRead() || !file.isFile())
	            System.exit(0);

	            BufferedReader in = null;
	        try {
	            in = new BufferedReader(new FileReader(datName));
	            String zeile = null;
	            while ((zeile = in.readLine()) != null) {
	             String eintrag1 = in.readLine();
	             String eintrag2 = in.readLine();
	             String eintrag3 = in.readLine();	
	             String eintrag4 = in.readLine();	
	             
	             SimpleDateFormat meinDatumsformat = new SimpleDateFormat("dd.mm.yyyy");
	             Date meinDatum = (Date) meinDatumsformat.parse(eintrag1);
	             
	             Integer i = 0;
	             Integer.parseInt(eintrag4);
	             
	             Lerneintrag l1 = new Lerneintrag(meinDatum, eintrag2,eintrag3, i);
	             return l1;
	             
	             
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (in != null)
	                try {
	                    in.close();
	                } catch (IOException e) {
	                }
	        }
			return null; 
        
	 }
	
}
