import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;

public class LerneintragVerwalter {
  
  protected static ArrayList<Lerneintrag> Lerneintraglist = new ArrayList<Lerneintrag>();
  
 
  public static void erstelle(Date datum, String fach, String beschreibung, int dauer){
    Lerneintrag hilfslern  = new Lerneintrag(datum, fach, beschreibung, dauer);
    Lerneintraglist.add(hilfslern);
  }

  public static ArrayList<Lerneintrag> getMP3Liste() {
    return Lerneintraglist;
  }

   
  public static void loesche(int index){
	  Lerneintraglist.remove(index);   
  }  
  
  
//  public static void schreibeLerneintragListe(String dateiname){
//    PersistenzSerialisierungMp3.serialisiere(dateiname);
//  }
  
  
  public static ArrayList<Lerneintrag> liesListe(String dateiname) throws ParseException{
    DateiEinlesen.ladeDatei(dateiname);
    return Lerneintraglist;
  } 
  
  
  public static String zeigeAlleEintraeg(){
    String tags = Lerneintraglist.toString();
    return tags;
  } 
} 
