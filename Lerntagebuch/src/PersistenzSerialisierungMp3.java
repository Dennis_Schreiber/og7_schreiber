/**
  *
  * Klasse zum Serialisieren und Deserialisieren
  * von ArrayLists bestimmter Objekttypen hier MP3
  * Die Methoden verf�gt �ber Klassenmethoden, also Aufruf �ber Klasse.methode() 
  *
  * @version 1.0 vom 11.05.2016
  * @author Francioni 
  */

import java.io.*;
import java.util.ArrayList;

public class PersistenzSerialisierungMp3 implements Serializable{
  
  //private static ArrayList<Mp3> mp3liste = new ArrayList<Mp3>();
  
  
  /** Schreibt ein serialisiertes Objekt vom Typ ArrayList mit 
    * Mp3-Objekten weg.
    * Da die Liste im Verwalter protected ist, kann direkt zugegriffen werden,
    * es braucht keinen Parameter ;-)
    * @param dateiname Name der Datei, die geschrieben wird
   */
  public static void serialisiere (String dateiname) {
    try {
      /**Objekt serialisieren*/
      FileOutputStream outStream = new FileOutputStream(dateiname);
      ObjectOutputStream ooutStream = new ObjectOutputStream(outStream);
      
      ooutStream.writeObject(Mp3Verwalter.mp3liste);
      
      outStream.close();
    }
    
    catch (IOException e){
      System.out.println("Schreiben nicht m�glich!" + e);
    }
  }
  
  /** Liest ein serialisiertes Objekt vom Typ ArrayList mit Mp3-Objekten.
    * Da die Liste im Verwalter protected ist, kann direkt zugegriffen werden,
    * es braucht keinen Parameter ;-)
    * @param dateiname Name des serialisierten Objekts, das gelesen wird
   */
  public static void deserialisiere(String datei) {
    
    try {
      FileInputStream inStream = new FileInputStream(datei);
      ObjectInputStream oinStream = new ObjectInputStream(inStream);
      
      Mp3Verwalter.mp3liste = (ArrayList<Mp3>)oinStream.readObject();
      
      oinStream.close();
      
    }
    catch (ClassNotFoundException e) {
      System.out.println("Objekt-Lesefehler!");
    }
    catch (IOException e){
      System.out.println("Lesen nicht m�glich!" + e);
    }
    
    //return mp3liste;
  }
} // end of class PersistenzSerialisierungMp3
