
public class Tester {

	public static void main(String[] args){
	
	
			 int laenge = 20000000; 
		
		long[] cool = new long[laenge];
		
		for(int i = 0; i < cool.length; i++) {
			cool[i] = i + 1;
		}
		
		Zahlensuche zs = new Zahlensuche();

		long anfang = 0;
		long ende = cool.length - 1;
		long normZahl = 10000000;
		long worstZahl = 20000000;
		long bestZahl = 1;
		long nichtZahl = 200000002;
		
		Stoppuhr st = new Stoppuhr();
		
		System.out.println("normale Zahl");
		for(int r = 0; r < 5; r++) {
		st.start();
		System.out.println(zs.searchInter(cool, anfang, ende, normZahl));
		st.stopp();
		System.out.println(st.getMillisek() + "ms inter");
		st.reset();
		
		st.start();
		for(int i = 0; i < cool.length; i++)
			if(cool[i] == normZahl)
				System.out.println(i);
		st.stopp();
		System.out.println(st.getMillisek() + "ms lin");
		st.reset();
		}
		
		System.out.println("worst case Zahl");
		for(int r = 0; r < 5; r++) {
			st.start();
			System.out.println(zs.searchInter(cool, anfang, ende, worstZahl));
			st.stopp();
			System.out.println(st.getMillisek() + "ms inter");
			st.reset();
			
			st.start();
			for(int i = 0; i < cool.length; i++)
				if(cool[i] == worstZahl)
					System.out.println(i);
			st.stopp();
			System.out.println(st.getMillisek() + "ms lin");
			st.reset();
			}
		
		System.out.println("best case Zahl");
		for(int r = 0; r < 5; r++) {
			st.start();
			System.out.println(zs.searchInter(cool, anfang, ende, bestZahl));
			st.stopp();
			System.out.println(st.getMillisek() + "ms inter");
			st.reset();
			
			st.start();
			for(int i = 0; i < cool.length; i++)
				if(cool[i] == bestZahl)
					System.out.println(i);
			st.stopp();
			System.out.println(st.getMillisek() + "ms lin");
			st.reset();
			}
		
		System.out.println("nicht enthalten Zahl");
		for(int r = 0; r < 5; r++) {
			st.start();
			System.out.println(zs.searchInter(cool, anfang, ende, nichtZahl));
			st.stopp();
			System.out.println(st.getMillisek() + "ms inter");
			st.reset();
			
			st.start();
			for(int i = 0; i < cool.length; i++)
				if(cool[i] == nichtZahl)
					System.out.println(i);
			st.stopp();
			System.out.println(st.getMillisek() + "ms lin");
			st.reset();
			}
		
		
			   
	}

}
