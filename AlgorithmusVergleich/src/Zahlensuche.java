

public class Zahlensuche {
	 
	 public long searchInter(long[] Feld, long links, long rechts, long zahl) { 

	        long bereich = Feld[(int) rechts] - Feld[(int) links]; 
	        long grenze = links 
	                + (int) (((double) rechts - links) * (zahl - Feld[(int) links]) / bereich);
 

	        if (grenze >= Feld.length) {  
	            return -1; 
	        } 
	       
	        if (zahl > Feld[(int) grenze]) { 
	            searchInter(Feld, grenze + 1, rechts, zahl); 
	        } else if (zahl < Feld[(int) grenze] && links != grenze) { 
	            searchInter(Feld, links, grenze - 1, zahl); 
	        } else if (zahl == Feld[(int) grenze]) { 
	            return grenze; 
	        } else { 
	            return -1; 
	        }
			return grenze; 
	    } 
	 
	 
	 
}
