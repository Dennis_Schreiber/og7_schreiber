
public class Stoppuhr {
	//Relevanz=WICHTIG
	private long start;
	private long stopp;
	
	public void start() {
		start = System.currentTimeMillis();
	}
	
	public void stopp() {
		stopp = System.currentTimeMillis() - start;
	}
	
	public void reset() {
		start = 0;
		stopp = 0;
	}
	
	public long getMillisek() {
		return stopp;
	}
}
