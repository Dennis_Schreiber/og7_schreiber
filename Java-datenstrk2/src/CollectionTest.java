

import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		String eintrag2;
		String eintrag3;
		
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':	
                System.out.println("Name des Autors");
                eintrag = myScanner.next();
                System.out.println("Buchtitel");
                eintrag2 = myScanner.next();
                System.out.println("Isbn");
                eintrag3 = myScanner.next();
                CollectionTest.f�ghinz(buchliste, eintrag, eintrag2, eintrag3);
				break;
			case '2':
				System.out.println("Die Isbn des gesuchten Buches?");
				eintrag = myScanner.next();
				System.out.println(CollectionTest.findeBuch(buchliste, eintrag));
				break;
			case '3':
				System.out.println("Die Isbn des zu l�schenden Buches?");
				eintrag = myScanner.next();
				CollectionTest.loescheBuch(buchliste, eintrag);		
				break;
			case '4':
				System.out.println("Die Gr��te Isbn ist " + CollectionTest.ermitteleGroessteISBN(buchliste));
				break;		
			case '5':
				System.out.println(CollectionTest.zeig(buchliste));
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 9);
}// main

	
	public static void f�ghinz(List<Buch> buchliste, String autor, String titel, String isbn) {
		Buch b1 = new Buch(autor,titel,isbn);
		buchliste.add(b1);
	}
	
	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		
		for(int i = 0; i < buchliste.size(); i++) {
			if(buchliste.get(i).getIsbn().equals(isbn)) {
				return buchliste.get(i);
			}
		}
		return null;	
	}

	public static boolean loescheBuch(List<Buch> buchliste, String isbn) {
		
		for(int i = 0; i < buchliste.size(); i++) {
			if(buchliste.get(i).getIsbn().equals(isbn)) {
				buchliste.remove(i);
				return true;
			}
		}
		return false;
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		String ta ="";
		Buch temp = null;
		for(int i = 1; i < buchliste.size();i++){
			for(int j = 0; j < buchliste.size()-1;j++) {
				if(buchliste.get(j).compareTo(buchliste.get(j + 1)) > 0) {
					temp = buchliste.get(j);
					buchliste.set(j, buchliste.get(j+1));
					buchliste.set(j+1, temp);
				}
				
			}
		}
		ta = buchliste.get(buchliste.size()-1).getIsbn();
		return ta;
	}

	public static String zeig(List<Buch> buchliste) {
		String n = "";
		for(int i = 0; i<buchliste.size();i++)
		n = buchliste.toString();
		return n;
	}
}
